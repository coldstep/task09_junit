package TaskTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeFalse;


import com.task09_JUNIT.task.Person;
import com.task09_JUNIT.task.InfoAboutPerson;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FirstTest {

  private static InfoAboutPerson infoAboutPerson;

  @BeforeAll
  static void init() {
    Person person = new Person();
    person.setName("Bruce");
    person.setSurname("Willis");
    person.setAge(50);
    person.setSingle(true);
    infoAboutPerson = new InfoAboutPerson(person);
    System.out.println("Welcome ))");
  }

  @BeforeEach
  void testBeforeEach() {
    System.out.println("Begin test");
  }

  @AfterEach
  void testAfterEach() {
    infoAboutPerson.getPerson().setName("Bruce");
    infoAboutPerson.getPerson().setSurname("Willis");
    infoAboutPerson.getPerson().setAge(50);
    System.out.println("Test finish");
  }

  @AfterAll
  static void byeBye() {
    System.out.println("Bye bye )))");
  }

  @Test
  @DisplayName("1")
  void testAssertEquals() {
    assertEquals("java.lang.String",
        infoAboutPerson.getPerson().getName().getClass().getTypeName());
  }

  @Test
  @DisplayName("2")
  void testAssertBooleanAndAssume() {
    assumeFalse(infoAboutPerson.getPerson().isSingle());
    assertTrue(infoAboutPerson.getPerson().isSingle());
  }


  @Test
  @DisplayName("3")
  void testAssertNull() {
    infoAboutPerson.getPerson().setSurname(null);
    assertNull(infoAboutPerson.getPerson().getSurname());
  }

  @Test
  @DisplayName("4")
  void testAssertThrows() {
    assertThrows(IllegalArgumentException.class,
        () -> {
          throw new IllegalArgumentException("Just a test");
        }
    );
  }

  @Test
  @DisplayName("5")
  void testAssertAll() {
    assertAll("all",
        () -> assertEquals("Bruce", infoAboutPerson.getPerson().getName()),
        () -> assertEquals("Willis", infoAboutPerson.getPerson().getSurname()),
        () -> assertEquals(40, infoAboutPerson.getPerson().getAge()),
        () -> assertFalse(infoAboutPerson.getPerson().isSingle()),
        () -> assertEquals("IT specialist", Person.getProffesion()),
        () -> assertEquals("Gorodocka street", Person.getAddress())
    );
  }

  @Test
  @DisplayName("Test void method")
  void testVoidMethod() {
    String temp = infoAboutPerson.getPerson().getName();
    infoAboutPerson.changeName();
    assertEquals(temp, infoAboutPerson.getPerson().getName());
  }

}
