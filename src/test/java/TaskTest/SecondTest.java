package TaskTest;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.task09_JUNIT.task.Person;
import com.task09_JUNIT.task.InfoAboutPerson;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class SecondTest {

  private static InfoAboutPerson infoAboutPerson;
  private static Person person;

  @BeforeAll
  static void init() {
    person = mock(Person.class);
    infoAboutPerson = new InfoAboutPerson(person);
    when(infoAboutPerson.getPerson().getName()).thenReturn("Bruce");
    when(infoAboutPerson.getPerson().getSurname()).thenReturn("Willis");
    when(infoAboutPerson.getPerson().getAge()).thenReturn(50);
    when(infoAboutPerson.getPerson().isSingle()).thenReturn(true);

    System.out.println("Test is beginning");
  }


  @Test
  @DisplayName("1")
  void testMocks() {
    assertAll(
        () -> assertEquals("Bruce", infoAboutPerson.getPerson().getName()),
        () -> assertNotNull(infoAboutPerson.getPerson().getSurname()),
        () -> assertEquals("Willis", infoAboutPerson.getPerson().getSurname()),
        () -> assertEquals(50, infoAboutPerson.getPerson().getAge()),
        () -> assertTrue(infoAboutPerson.getPerson().isSingle())
    );

    verify(infoAboutPerson.getPerson()).getName();
  }

  @AfterAll
  static void end() {
    System.out.println("Test was finished !");
  }

}
