package com.task09_JUNIT.task;

public class Person {

  private String name;
  private String surname;
  private int age;
  private boolean isSingle;
  public static final String proffesion = "java developer";
  protected static final String address = "Gorodocka street";

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean isSingle() {
    return isSingle;
  }

  public void setSingle(boolean single) {
    isSingle = single;
  }

  public static String getProffesion() {
    return proffesion;
  }

  public static String getAddress() {
    return address;
  }

  @Override
  public String toString() {
    return "Person{" +
        "name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        ", age=" + age +
        ", isSingle=" + isSingle +
        ", profession=" + proffesion +
        ", address=" + address +
        '}';
  }
}
