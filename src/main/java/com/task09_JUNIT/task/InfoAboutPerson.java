package com.task09_JUNIT.task;

public class InfoAboutPerson {

  private Person person;

  public InfoAboutPerson() {
  }

  public InfoAboutPerson(Person person) {
    this.person = person;
  }

  public void showResult() {
    System.out.println(person.toString());
  }

  public void changeName(){
    person.setName("Bill");
  }
  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

}
